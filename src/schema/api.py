from tastypie.resources import Resource
from django.conf.urls import url
from tastypie.api import Api
from django.http.response import HttpResponse
import json
import os
import importlib
from inspect import isclass
import traceback

class SchemaResource(Resource):
    class Meta:        
        list_allowed_methods = ['get']
        detail_allowed_methods = []    
    
    def prepend_urls(self):
        return [ url(r"^schema.js$", self.wrap_view('get_schema'), name="api_user_login") ]
    
    
    def get_schema(self, request, **kwargs):
        self.method_check(request, allowed=['get'])        
        varname = request.GET.get('varname','model_schema')
                
        schema = {}
        
        for model, res in api._registry.iteritems():            
            fields = res.build_schema()['fields']                        
            schema[model] = {}
            for field_name, field_object in res.fields.items():
                ftype = fields[field_name]['type']                
                schema[model][field_name] = {}
                if field_object.dehydrated_type == 'related':
                    ftype = field_object.to_class().get_resource_uri()
                    if getattr(field_object, 'is_m2m', False):                        
                        schema[model][field_name]['is_list'] = True
                schema[model][field_name]['type'] = ftype                
        
        return HttpResponse(content="window.%s = %s ;" % (varname, json.dumps(schema)), content_type = "application/javascript")


try:
    settings = importlib.import_module(os.getenv("DJANGO_SETTINGS_MODULE", None))
except Exception, ex:
    raise Exception("Error occured when loading settings: %s" % str(ex))

print settings.API_NAME

api = Api(api_name='v1')

#Get all apps
APPS = settings.INSTALLED_APPS[settings.INSTALLED_APPS.index("users"):]

print "APPS:", APPS

def import_apis(app):
    try:
        exec "from %s.api import *" % (app)
    except Exception, ex:
        print "Skipping app '%s' due to error: %s" %(app, str(ex))
        traceback.print_exc() 
    for r in locals().values():        
        if isclass(r) and issubclass(r, Resource) and r.__module__ == '%s.api' %(app):
            print "Adding RESOURCE::", r
            api.register(r())

map(import_apis, APPS)
