from tastypie.authentication import ApiKeyAuthentication
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, Resource

from users.api import UserResource
from tastypie import fields

class CustomMeta:
    authentication = ApiKeyAuthentication()
    authorization = Authorization()
    allowed_methods = ['get','put','post']
    
class CreatedByHelper:
    #created_by = fields.ForeignKey(UserResource, 'created_by')
    
    def hydrate_created_by(self, bundle):        
        res = UserResource()        
        bundle.data['created_by'] = res.get_resource_uri(bundle.request.user)        
        return bundle
        