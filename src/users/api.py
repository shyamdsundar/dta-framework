from django.conf.urls import url
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.db import models
from tastypie.authentication import ApiKeyAuthentication, \
    Authentication
from tastypie.authorization import Authorization
from tastypie.http import HttpForbidden, HttpUnauthorized, HttpBadRequest
from tastypie.models import create_api_key, ApiKey
from tastypie.resources import ModelResource
from django.conf import settings
from tastypie import fields

class UserAuthorization(Authorization):
    def read_detail(self, object_list, bundle):
        if object_list.filter(username = bundle.request.user).exists():
            return True;
        return False;

class UserResource(ModelResource):  
    if hasattr(settings, "USER_PROFILE_RESOURCE"):          
        profile = fields.ForeignKey(*settings.USER_PROFILE_RESOURCE, null=True, full=True)
    #profile = fields.ForeignKey(*settings.USER_PROFILE_RESOURCE)
    
    #def dehydrate_profile(self, bundle):        
    #    #this function is called only if profile is present in User        
    #    profile, created = User.profile.related.related_model.objects.get_or_create(user = bundle.obj)                                                    
    #    return self.profile.to_class().build_bundle(obj = profile)
    
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        excludes = ['email', 'password', 'is_superuser', 'last_login', 'date_joined']        
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        detail_uri_name = 'username'
    
    def prepend_urls(self):
        return [
            url(r"^login/$", self.wrap_view('login'), name="api_user_login"),
            url(r"^register/$", self.wrap_view('register'), name="api_user_register"),            
        ]
    
    def login(self, request, **kwargs):
        self.method_check(request, allowed=['post'])        
        data = self.deserialize(request, request.body, format=request.META.get('CONTENT_TYPE', 'application/json'))

        username = data.get('username', data.get('email', None))            
        password = data.get('password', None)
        
        if username is None or password is None:
            return self.create_response(request, {
                    'success': False,
                    'reason': 'incompletedata',
                    'reason_desc': 'Need username/email and password.',
                    }, HttpBadRequest )
                    
        
        user = User.objects.filter( models.Q(username = username) |  models.Q(email = username))
        if user.exists():
            username = user[0].username
        else:
            return self.create_response(request, {
                    'success': False,
                    'reason': 'notfound',
                    'reason_desc': 'User is not registered.',
                    }, HttpForbidden )            
        
        user = authenticate(username=username, password=password)
        
        if user:
            if user.is_active:
                login(request, user)
                return self.create_response(request, {
                    'username': username,
                    'token': ApiKey.objects.get_or_create(user=user)[0].key
                })
            else:
                return self.create_response(request, {
                    'success': False,
                    'reason': 'disabled',
                    'reason_desc': 'User has been disabled.',
                    }, HttpForbidden )
        else:
            return self.create_response(request, {
                'success': False,
                'reason': 'incorrect',
                'reason_desc': 'Incorrect username or password.',
                }, HttpUnauthorized )
    
    def register(self, request, **kwargs):
        self.method_check(request, allowed=['post'])        
        data = self.deserialize(request, request.body, format=request.META.get('CONTENT_TYPE', 'application/json'))
        
        if User.objects.filter(username = data['username']):
            return self.create_response(request, {
                                                  'error':'Username taken'
                                                  }, HttpForbidden)
        if User.objects.filter(email = data['email']):
            return self.create_response(request, {
                                                  'error':'Email already registered'
                                                  }, HttpForbidden)                     
        
        user = User.objects.create_user(username = data['username'], 
                                        email = data['email'], password = data['password'])
        
        if hasattr(settings, "USER_PROFILE_RESOURCE"):
            profile, created = User.profile.related.related_model.objects.get_or_create(user=user, defaults = data['profile'])            
        
        return self.create_response(request, {
                    'username': user.username,
                    'token': ApiKey.objects.get_or_create(user=user)[0].key
                })
        
        
        
        
# class UserRegistrationResource(ModelResource):
#     class Meta:        
#         object_class = User
#         queryset = User.objects.all()
#         resource_name = 'register'        
#         allowed_methods = ['post']        
#         authentication = Authentication()
#         authorization = Authorization()
#         #always_return_data = True
#     
#     def obj_create(self, bundle, **kwargs):                
#         User.objects.create_user(username = bundle.data['username'], 
#                                         email = bundle.data['email'], password = bundle.data['password'])
        

    

models.signals.post_save.connect(create_api_key, sender=User)
