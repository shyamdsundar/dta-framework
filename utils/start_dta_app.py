#!/usr/bin/python
import os
import argparse
import sys

templates = {
'controllers':"""GenericController = dta.GenericController
angular.module '{APPNAME}.{MODNAME}.controller', ['{APPNAME}.{MODNAME}.service']
    .controller 'DummyController',
        class DummyController extends GenericController
            @$inject: GenericController.$inject.concat []
""",
'directives':"""angular.module '{APPNAME}.{MODNAME}.directive', ['{APPNAME}.{MODNAME}.service']
    .directive '{APPNAME}.{MODNAME}_directive', ->
        restrict:'AE'
        templateUrl:'templates/report/...html'
        scope:
            variable:'='
        controller:'tmp'
""",
'module':"""angular.module '{APPNAME}.{MODNAME}', ['{APPNAME}.{MODNAME}.service', '{APPNAME}.{MODNAME}.controller', '{APPNAME}.{MODNAME}.directive']
    .config ($stateProvider) ->
        $stateProvider
        #    .state 'report',
        #                url:'/report'
        #                templateUrl:'/templates/report/report.html'
""",
'services':"""angular.module '{APPNAME}.{MODNAME}.service', ['dta.models']
    .factory 'DummyService', (ApiService)->
        class DummyService extends ApiService
            model:null
        new SummaryService()
"""
}


parser = argparse.ArgumentParser()
parser.add_argument('--app', 
                        default="myapp",      
                        required=False,
                        help='Name of app')
parser.add_argument('--module', 
                        required=True,
                        help='Name of module')

args = parser.parse_args()

if os.path.exists(args.module):
	print "Module path already exists. Skipping creation."
	sys.exit()

os.mkdir(args.module)

for k,v in templates.iteritems():
	with open(os.path.join(args.module, k+'.coffee'), 'w') as fp:
		print >>fp, v.format(APPNAME=args.app, MODNAME=args.module)

print "Done!"
