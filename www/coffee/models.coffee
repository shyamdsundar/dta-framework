dateParser = (val)->
    if typeof(val) == 'string'
        return new Date(Date.parse(val))
    if Object.prototype.toString.call(val) == "[object Date]"
        return val

_modelArrayFunc = (proto)=> (obj)->
            pv = _parseValue({type:proto.type}, obj)
            Array.prototype.push.call @, pv

_updateArray = (destination, objects)->
                if destination is null or destination is undefined
                    console.error "UpdateArray called without source"
                    return
                sameindex = null

                for i in [0..(destination.length-1)]
                    if destination[i] == objects[i]
                        sameindex = i
                if sameindex != null
                    if sameindex == (destination.length-1) and objects.length > destination.length
                        console.log "SAMEINDEX PUSH", sameindex, destination.length, objects.length
                        for i in [destination.length..(objects.length-1)]
                            #destination.push objects[i]
                            destination.splice(i + 1, 0, objects[i])
                            #destination.splice(2 + 1, 0, objects[i])






parseBoolean = (val)->
    if not val
        return val
    if typeof(val) == 'string'
        val = val.toLowerCase()
        if val == "true"
            return true
        else if val == "false"
            return false
        else
            throw "Unrecognized boolean value as string"
    else if val == true or val == false
            return val
    else
        throw "Unrecognized boolean value"


mergeProperty = (property1, property2)->
    newprop = {}
    for k,v of property1
        newprop[k] = v
    for k,v of property2
        newprop[k] = v
    newprop


identityParser = (val)->val
dictParser = (val)->
    val
_parserMapping =
    string:identityParser
    null:identityParser
    undefined:identityParser
    integer:parseInt
    float:parseFloat
    datetime:dateParser
    boolean:parseBoolean
    dict:dictParser

_parseValue = (proto, val)->
    if val == null or val == undefined
        return val
    type = proto.type
    if proto.is_list
        if not val
            val = []
        if Object.prototype.toString.call(val) != "[object Array]"
            throw "Need an array for list type"
        ma = []
        for v in val
            ma.push _parseValue({type:type}, v)
        return ma
    if _parserMapping[type]
        return _parserMapping[type](val)
    if typeof(type) == 'function'
        if type.prototype.URL #this is a model
            m = type.get_or_create(val)
            if m == undefined
                console.error "UNDEFINED::", val
            return m
        else
            return type(val)
    #console.log "PROTO", proto, typeof(proto.type)
    console.error "Unhandled type", proto, typeof(type), val
    throw "Unhandled type"

__model_cache = {}
window.config.model_cache = __model_cache
getCachedModel = (data)->
    #check if data is already created
    if data.resource_uri
        data = data.resource_uri
    if typeof(data) == 'string' and data.search('/api')>=0
        data = data.slice(data.search("/api/"))
        if data.slice(-1) != "/"
            data += "/"
    if __model_cache[data]
        #console.log "Returning cache for ", data
        return __model_cache[data]
    #console.log "Caching:", data
    return


storeModelCache = (url, model)->
    if typeof(url) != 'string'
        if url.resource_uri
            url = url.resource_uri
        else if url.id
            id = url.id
            url = model.BASEURL + model.URL
            if url.slice(-1) != "/"
                url += "/"
            url += "#{id}/"
            console.log "URL WITH ID:", url
        else
            console.info "Not caching. Should be a new object.", model.URL
            return
            #throw "Unrecognized data"
    else
        url = url.slice(url.search("/api/"))
        if url.search("\\?")>=0
            #this is a list query with parameters. Dont do anything
            console.warn "Query #{url} cannot be cached"
            return
        if url.slice(-1) != "/"
            url += "/"
        #console.log "Storing cache", url
    #if __model_cache[url] and __model_cache[url] != model
    #    console.error "Overwriting cache for #{url}"
    __model_cache[url] = model

window.model_cache = __model_cache

angular.module(window.config.APPNAME+'.models', [])
    .config ($httpProvider)->
        if not ('authHttpInterceptor' in $httpProvider.interceptors)
            $httpProvider.interceptors.push 'authHttpInterceptor'

    .factory 'authHttpInterceptor', ($q)->
            request:(config)->
                try
                    credentials = JSON.parse window.localStorage['login.credentials']
                    config.headers['Authorization'] = "ApiKey #{credentials.username}:#{credentials.token}"
                    config.headers['Content-Type'] = 'application/json'
                catch
                    credentials = null
                config
            #responseError: (rejection)->
            #    console.log "REJECTION:", rejection
            #    $q.reject rejection

    .factory 'ApiModel', ($http)->
        __obj_count = 0
        class ApiModel
            @SCHEMA:null

            @get_or_create:(data, noupdate)->

                if data instanceof @
                    return data

                url = data
                idlabel = @::ID

                if url and url.resource_uri #could be the data containing url
                    url = url.resource_uri
                else if url and url.hasOwnProperty(idlabel)
                        if url[idlabel] != null and url[idlabel]!=undefined and (not url[idlabel]<0)
                            url = url[idlabel]
                        else
                            return new @(data)

                if typeof(url) == 'number'
                    url = String(url)
                if typeof(url) != 'string'
                    #this should be an object
                    if typeof(url) == 'object'
                        console.log "Received data for get_or_create"
                        return new @(url)
                    else
                        console.error url
                        throw "Unknown type received for get_or_create"
                #if not isNaN(parseInt(url)) #this is a number
                if String(url).search("/") < 0 #this is not a URL. Should be an ID
                    id = url
                    url = @::BASEURL + @::URL
                    if url.slice(-1) != "/"
                        url += "/"
                    url += "#{id}/"

                #console.log "Request URL:", url
                if (cached = getCachedModel(url))
                    #we got the cached model.
                    if noupdate
                        return cached
                    #one easy way to check if data is from the server is to check for 'resource_uri'. If it is from server, we need to update it with the data
                    if data.resource_uri
                        cached.parse data
                    return cached
                return new @(data)

            HOSTURL:'http://localhost:8000'
            BASEURL:'/api/v1'
            URL:null
            ID:'id'
            constructor:(data)->
                @__data = {}
                @_success_funcs = []
                @_error_funcs = []
                @_ready_funcs = []
                @_ready = true
                @__mid = __obj_count
                __obj_count += 1

                create_getter = (key, proto)=> => @__data[key]
                create_setter = (key, proto)=> (val)=>
                                try
                                    if val == undefined
                                        if @__data.hasOwnProperty(key)
                                            delete @__data[key]
                                        return
                                    pval = _parseValue(proto, val)
                                    if pval instanceof Array
                                        if not @__data[key]
                                            @__data[key] = []
                                            #@__data[key].push = _modelArrayFunc(proto)
                                        _updateArray @__data[key], pval
                                    else
                                        @__data[key] = pval
                                catch
                                    console.error "ERROR IN ASSIGNING PROPERTY", @

                schema = @_getSchema()
                if schema is undefined
                    schema = {}
                if schema[@ID] == undefined
                    schema[@ID] = 'string'
                for k,v of schema
                    Object.defineProperty @, k,
                            get:create_getter(k,v)
                            set:create_setter(k,v)

                #store the model in memory as we have created it just now
                if data
                    @parse data

            _getSchema:->
                if @SCHEMA
                    return @SCHEMA
                url = @URL.slice(1)
                model_schema[url]


            parse:(data)->
                #first store the cache.
                storeModelCache data, @
                if typeof data == 'string'
                    if data.search('/api')<0
                        #console.error "Invalid URL '#{data}'to parse"
                        url = @BASEURL + @URL + "/" + data
                        if not url.endsWith("/")
                            url += "/"
                    else
                        url = data.slice(data.search("/api/"))
                    #if getCachedModel(@HOSTURL + data) and getCachedModel(@HOSTURL + data) != @
                    #    console.error @
                    #    throw "Model being recreated."
                    @get
                        url:@HOSTURL + url
                        error:=>
                            console.error "Error while fetching model", @
                    return
                else
                    for k,v of data
                        @[k] = v

                @

            getID:-> @[@ID]
            getUri:(id)->
                id = id or @[@ID]
                url = @BASEURL + @URL + '/'
                if id != undefined and id != null
                    return url + "#{id}/"
                else
                    return url

            getURL:(id)->
                return @HOSTURL + @getUri(id)

            onReady:(func)->
                if @_ready
                    func.apply @
                    return
                if not @_ready_funcs
                    @_ready_funcs = []
                @_ready_funcs.push func

            __executeReadyFuncs:()=>
                @_ready = true
                while @_ready_funcs.length > 0
                    @_ready_funcs.pop().apply @

            __executeSuccessFuncs:(response)=>
                @_error_funcs = []
                while @_success_funcs.length > 0
                    @_success_funcs.pop().apply @, [response]
                @__executeReadyFuncs()

            __executeErrorFuncs:(response)=>
                @_success_funcs = []
                while @_error_funcs.length > 0
                    @_error_funcs.pop()(response)
                @__executeReadyFuncs()

            onAllReady:(func, maxdepth)->
                if maxdepth is undefined
                    maxdepth = 30
                if maxdepth == 0
                    console.log "Max depth reached on Ready"
                    return func.call @

                pending_count = 0
                @onReady =>
                    is_pending = false
                    for key,v of @_getSchema()
                        if @[key] and @[key].getUri != undefined
                            if @[key]._ready
                                continue
                            pending_count += 1
                            is_pending = true
                            childfunc = =>
                                pending_count -= 1
                                if pending_count == 0
                                    func.call @
                            @[key].onAllReady childfunc, (maxdepth-1)

                    if not is_pending
                        #the request is not pending. So, execute the function
                        return func.call @


            get:({data, success, error, final, url}={})->
                id = @[@ID]
                if url and url.hasOwnProperty(@ID)
                    id = url[@ID]
                if typeof(url) == 'string' and url.endsWith('-1/')
                    console.error "INVALID URL"
                if typeof(url) != 'string' and (id == null or id == undefined or id<0)
                    if not url and not data
                        console.error "Nothing to get"
                    console.log "Not getting #{@URL} as #{@ID} '#{id}' is not valid"
                    return

                _url = url or @getURL()

                if success
                    @_success_funcs.push success
                if error
                    @_error_funcs.push error

                if @_ready == false
                    console.warn "Duplicate gets triggered", _url
                    return
                @_ready = false

                _success = (response)=>
                            @parse response.data
                            @__executeSuccessFuncs(response)
                _failure = (response)=>
                            @__executeErrorFuncs(response)
                _finally = ()=>
                            final and final()
                            @__executeReadyFuncs() #just to be sure. TODO: Check and remove this line

                $http.get _url
                    .then(_success, _failure).finally(_finally)


            _prepareDataForPost:(data)->
                if data == undefined
                    data = @__data
                    window.test33 = @
                #console.log "DATA", data, (data instanceof ApiModel)

                if @_ready == false
                    console.error "POST called when model is not ready"

                if Object.prototype.toString.call(data) == "[object Array]"
                    adata = []
                    for a in data
                        adata.push @_prepareDataForPost a
                    return adata
                if data.constructor and data.constructor.prototype.URL
                    console.log "API MODEL"
                    if data.resource_uri != undefined
                        return data.resource_uri
                    console.log "API MODEL", data
                    data = data._prepareDataForPost()
                if Object.prototype.toString.call(data) == "[object Object]"
                    newdata = {}
                    for k,v of data
                        if v not in [null, undefined]
                            v = @_prepareDataForPost v
                        newdata[k] = v
                    data = newdata
                data

            save:({data, success, error, final, url, method}={})->
                _url = url or @getURL()
                _success = (response)=>
                            if response.status = 201 and response.data and response.data.resource_uri
                                @parse response.data

                            success and success(response)
                            @__executeReadyFuncs()
                _failure = (response)->
                            error and error(response)
                _finally = ()->
                            final and final()

                if data == undefined
                    data = @__data

                console.log data

                data = @_prepareDataForPost data

                if not method
                    if @getID() in [null, undefined]
                        method = 'post'
                    else
                        method = 'put'

                console.log "DATA for #{method}:", JSON.stringify(data)

                fn = if method is 'post' then $http.post else $http.put

                fn _url, data
                    .then(_success, _failure)['finally'](_finally)

            post:(config)->
                config.method = 'post'
                @save config
            put:(config)->
                config.method = 'put'
                @save config


    .factory 'ApiService', ($http, AuthenticatedUserService)->
        class ApiService
            model:null
            constructor:()->
                @_change_funcs = []
                @objects = []

            getURL:->
                m = new @model()
                url = m.getURL()
                url

            get:({params, success, error, final, url}={})->
                if not url and not @model
                    console.error "Need a model class to continue"
                    return
                if not url
                    url = @getURL()

                _success = (response)=>
                            @parse response.data, success
                _failure = (response)->
                            error and error(response)
                _finally = ()->
                            final and final()

                $http.get url, {params:params}
                    .then(_success, _failure)['finally'](_finally)

            parse:(responsedata, success)->
                data = responsedata.objects
                if not (data instanceof Array)
                    throw "Data got to be an array"
                objects = []
                idorder = []
                count = data.length
                for d in data
                    idorder.push d.id
                    obj = @model.get_or_create(d)
                    obj.onReady do(obj)=>=>
                        objects.push(obj)
                        count -= 1
                        if count == 0
                            @setObjects objects.sort (a,b)-> (idorder.indexOf(a.id)-idorder.indexOf(b.id))
                            success and success(@objects)

            onChange:(func)->
                @_change_funcs.push func

            setObjects:(objects)=>
                @objects = objects
                while @_change_funcs.length > 0
                    console.warn "Deprecated logic"
                    @_change_funcs.pop().apply @
                @objects

            all:->
                console.warn "Deprecated logic"
                @objects

#this function needs to be called before app is initialized
config.initModels = ->

    url_map = {}
    schema = {}

    for f in angular.module('myapp.models')._invokeQueue
        if f[1] != 'factory'
            continue
        if f[2][0].slice(-7).toLowerCase() == 'service'
            continue
        cls = angular.injector(['ng', 'myapp.models']).get(f[2][0])
        if not cls.prototype
            continue
        url = cls.prototype.URL
        if not url
            continue
        url = url.slice(1)
        schema[url] = cls
        obj = new cls()
        url = obj.BASEURL + obj.URL
        if url.slice(-1) != "/"
            url += "/"
        url_map[url] = cls

    config.url_map = url_map
    config.models = schema

    urls = Object.keys(config.url_map)
    for name,schema of model_schema
        for propname, val of schema
            if val.type in urls
                val.type = config.url_map[val.type]

