window.config = window.config or {}
module = window.config

window.config.utils =
    formatDateToString : (val)->
        moment(val).format("DD-MM-YYYY HH:MM")

class module.GenericController
    @$inject: ['$rootScope', '$state', '$scope', '$modal', 'AuthenticatedUserService']

    applyScope:(scope)=>
        scope = scope or @_$scope
        if not scope
            console.error "Need a scope to apply"
        (not scope.$$phase and not scope.$root.$$phase) and scope.$apply()

    constructor:->
        for l,i in @constructor.$inject
            @['_'+l] = arguments[i]

        for k,v of window.config.utils
            @_$scope['_'+k] = v

        @_$scope.$on '$stateChangeSuccess', (ev, to, toParams, from, fromParams)->
            toParams.prevState = from:from,params:fromParams

        #now, we try to see if we are authenticated
        if @_AuthenticatedUserService.isAuthenticated()
            @init()
        else
            @showLoginPopup success:=>
                @init()

    init:->
        if @LISTSERVICE and (s = @['_'+@LISTSERVICE])
            @service = new s()
            @service.get
                success:(objects)=>
                    @_objects = objects
                error:(response)=>
                    console.log "ERROR", response
                    console.error "Error in getting objects"

            @getObjects = =>
                @_objects

    showAlert:({message,title, hasCancel, onCancel, onOk, onClose}={})->
        scope = @_$scope.$new()
        myPopup = @_$modal.open
            templateUrl: '/templates/alertdialog.html'
            animation: true
            scope: scope
            controller:($scope)=>
                $scope.$on 'modal.closing', ->
                    onClose and onClose()
                closefunc = ->
                    myPopup.close()
                $scope.onCancel = onCancel or closefunc
                $scope.onOk = onOk or closefunc
                $scope.hasCancel = (onCancel != undefined)
                $scope.message = message
                $scope.title = title




    showLoginPopup:({success}={})->
        handler = @getUnauthorizedErrorHandler success:success
        handler()


    getUnauthorizedErrorHandler:({success}={})=>
        fn = (response)=>
            if (not response) or response.status == 401
                console.log "Error in getting activities"
                popupScope = @_$scope.$new()
                popupScope.authenticatedUser = {username:'shyam', password:'shyam'}
                authservice = @_AuthenticatedUserService
                myPopup = @_$modal.open
                    templateUrl: '/templates/signinpopup.html'
                    animation: true
                    scope: popupScope
                    controller: ($scope)->
                        console.log "SIGNIN CTRL", $scope.authenticatedUser
                        $scope.close = ->myPopup.close()
                        watcher = -> JSON.stringify($scope.authenticatedUser)
                        $scope.$watch watcher, (oldval, newval)->
                            $scope.errorMessage = undefined
                        $scope.signIn = ()->
                            console.log "Signing in", $scope.authenticatedUser
                            authservice.authenticate
                                        username:$scope.authenticatedUser.username
                                        password:$scope.authenticatedUser.password
                                        success:->
                                            success and success.apply arguments
                                            $scope.close()
                                        error:(response)->
                                            if response.status == 401
                                                $scope.errorMessage = "Error occured: " + response.data.reason_desc
                                            else
                                                $scope.errorMessage = "Error (code #{response.status}) occured while signing in."




module.loadscript = (module, onload)->
    if module.slice(-2) != 'js'
        url = module.split(".")
        if url[0] != window.config.APPNAME
            onload and onload()
            return
        url[0] = 'js'
        url = url.join('/')
        if url.slice(-1)[0] != 'js'
            url += '.js'
    else
        url = module

    if not document.querySelector("script[src*='"+url+"']")
        head = document.getElementsByTagName("head")[0]
        script = document.createElement('script')
        script.setAttribute('src', url)
        script.setAttribute('type', 'text/javascript')
        if onload
            script.onload = onload
        head.appendChild(script)
    else
        onload and onload()

modulemap =
    'app':
        script:'js/app.js',
        deps:['models']
    'models':
        script:'js/models.js'


addModule = (modulename, componentstring, prefix, deps)->
    modulemap['app'].deps.push modulename
    common_deps = (deps and deps.slice()) or []
    if not prefix
        prefix = ""
    modulemap[modulename] =
            script:"js/#{prefix}#{modulename}/module.js"
            deps: common_deps.slice()
    cmap =
        C:'controllers'
        D:'directives'
        S:'services'
    for c in componentstring
        smod = "#{modulename}.#{cmap[c]}"
        modulemap[modulename].deps.push smod
        modulemap[ smod ] =
                script:"js/#{prefix}#{modulename}/#{cmap[c]}.js"
                deps: common_deps.slice()

createScriptTags = ->
    #create mapping first
    urls = {}
    for script,data of modulemap
        urls[script] = data.script

    #sort by dependencies
    ranks = []

    add_to_rank = (m)->
        if m in ranks
            return true
        if modulemap[m] is undefined
            console.error "Module #{m} is not defined"
        for d in (modulemap[m].deps or [])
            if d not in ranks
                add_to_rank d
        ranks.push m

    add_to_rank 'app'

    loadall = =>
        m = ranks.shift()
        if m
            console.log "LOADING:", m
            module.loadscript modulemap[m].script, loadall
    #console.log ranks
    for m in ranks
        module.loadscript modulemap[m].script

#load all defined modules
for modulename, mconfig of window.config.modules
    addModule modulename,mconfig

createScriptTags()

