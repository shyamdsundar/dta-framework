window.config = {}

window.config.APPNAME = 'myapp'

window.config.modules =
                user:'CSD'
                report:'CSD'
                request:'CSD'
                engine:'CSD'


window.config.modelConfig = ->
    angular.module('myapp.models')
        .factory 'User', (ApiModel)->
            class User extends ApiModel
                URL:"/user"
                ID:'username'
                getName:->
                    if @first_name
                        return @first_name
                    if @last_name
                        return @last_name
                    @username

        .factory 'DummyModel', (ApiModel)->
            class DummyModel extends ApiModel
                URL:'/modelurl'
                   

window.config.initApp = ->
    deps = ['ui.router', 'ui.bootstrap', 'smart-table']
    for m, tmp of window.config.modules
        deps.push "myapp.#{m}"

    angular.module window.config.APPNAME, deps
        .config ($stateProvider, $urlRouterProvider, $httpProvider) ->
            if not ('authHttpInterceptor' in $httpProvider.interceptors)
                $httpProvider.interceptors.push 'authHttpInterceptor'

            $urlRouterProvider.otherwise '/home'

            $stateProvider
                .state 'home',
                    url:'/home'
                    templateUrl:'templates/app/homepage.html'
                    controller:'SummaryController as controller'
                .state 'about',
                    url:'/about'
                    templateUrl:'templates/app/about.html'
        .controller 'RootController', ($element)->
            @
	

