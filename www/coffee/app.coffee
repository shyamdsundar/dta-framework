String::startsWith ?= (s) -> @[...s.length] is s
String::endsWith   ?= (s) -> s is '' or @[-s.length..] is s

angular.element(document).ready ->
    fn = ->
        window.config.modelConfig and window.config.modelConfig()
        window.config.initModels()
        window.config.initApp()
        angular.bootstrap(document, [window.config.APPNAME])
    #just to allow other Javascript files to finish running (other than loading)
    window.setTimeout fn, 0


